# -*- coding: utf-8 -*-

import os

from bopress import settings
from bopress.lib.basecache import SimpleCache, FileSystemCache
from bopress.utils import Utils

__author__ = 'yezang'


class GroupSimpleCache(SimpleCache):
    _paths = set()

    def __init__(self, threshold=500, default_timeout=300):
        SimpleCache.__init__(self, threshold, default_timeout)

    def set(self, path=list(), value=None, timeout=None):
        if not path:
            return
        key = "/".join(path)
        self._paths.add(key)
        super(GroupSimpleCache, self).set(key, value, timeout)

    def get(self, path=list(), default=None):
        key = "/".join(path)
        v = super(GroupSimpleCache, self).get(key)
        if not v:
            return default
        return v

    def get_many(self, path=list()):
        prefix = "/".join(path)
        cache_items = dict()
        for key in self._paths:
            if key.startswith(prefix):
                cache_items[key] = super(GroupSimpleCache, self).get(key)
        return cache_items

    def add(self, path=list(), value=None, timeout=None):
        if not path:
            return
        key = "/".join(path)
        self._paths.add(key)
        super(GroupSimpleCache, self).add(key, value, timeout)

    def delete(self, path=list()):
        prefix = "/".join(path)
        for key in self._paths:
            if key.startswith(prefix):
                super(GroupSimpleCache, self).delete(key)


class GroupFileCache(FileSystemCache):
    _paths = set()

    def __init__(self, cache_dir, threshold=500, default_timeout=300,
                 mode=0o600):
        FileSystemCache.__init__(self, cache_dir, threshold, default_timeout, mode)

    def set(self, path=list(), value=None, timeout=None):
        if not path:
            return
        key = "/".join(path)
        self._paths.add(key)
        super(GroupFileCache, self).set(key, value, timeout)

    def get(self, path=list(), default=None):
        key = "/".join(path)
        v = super(GroupFileCache, self).get(key)
        if not v:
            return default
        return v

    def get_many(self, path=list()):
        prefix = "/".join(path)
        cache_items = dict()
        for key in self._paths:
            if key.startswith(prefix):
                cache_items[key] = super(GroupFileCache, self).get(key)
        return cache_items

    def add(self, path=list(), value=None, timeout=None):
        if not path:
            return
        key = "/".join(path)
        self._paths.add(key)
        super(GroupFileCache, self).add(key, value, timeout)

    def delete(self, path=list()):
        prefix = "/".join(path)
        for key in self._paths:
            if key.startswith(prefix):
                super(GroupFileCache, self).delete(key)


class Cache(object):
    # file = type("file", (), {})
    # mem = type("mem", (), {})
    _instance = None
    _data = None
    _session = None

    @staticmethod
    def default():
        if Cache._instance:
            return Cache._instance
        if settings.CACHE_MODE == "simple":
            Cache._instance = GroupSimpleCache()
            return Cache._instance
        elif settings.CACHE_MODE == "file":
            path = Utils.mkdirs(os.path.join(settings.BASE_DIR, "cache", "temp"))
            Cache._instance = GroupFileCache(path)
            return Cache._instance
        return None

    @staticmethod
    def data():
        if Cache._data:
            return Cache._data
        path = Utils.mkdirs(os.path.join(settings.BASE_DIR, "cache", "data"))
        Cache._data = GroupFileCache(path)
        return Cache._data

    @staticmethod
    def session():
        if Cache._session:
            return Cache._session
        path = Utils.mkdirs(os.path.join(settings.BASE_DIR, "cache", "session"))
        Cache._session = GroupFileCache(path, settings.SESSION_STORE_MAX_ITEMS)
        return Cache._session
