# -*- coding: utf-8 -*-

import mimetypes
import smtplib
import socket
from email import encoders
from email.header import Header
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import parseaddr, formataddr

from tornado import gen
from tornado.queues import Queue

from bopress import options
from bopress import settings
from bopress.log import Logger

__author__ = 'yezang'


class MailerException(Exception):
    """ 邮件发送异常类 """
    pass


class NetworkError(MailerException):
    """ 网络异常类 """
    pass


class Mailer(object):
    @staticmethod
    def send_mail(to_address, subject, body, content_type="html", files=None):
        """
        发送邮件主程序
        :param content_type: `html` or `plain`
        :param to_address: 收件人邮箱
        :param subject: 邮件标题
        :param body: 邮件内容
        :param files: 附件
        :raise: NetworkError/MailerException
        """
        smtp_onfig = options.get_form_options("bo-smtp-settings")
        host = smtp_onfig.get('smtp_server')
        port = smtp_onfig.get('smtp_port')
        smtp_auth = smtp_onfig.get('smtp_auth')
        smtp_security = smtp_onfig.get('smtp_security')
        user = smtp_onfig.get('user_name')
        passwd = smtp_onfig.get('user_pass')
        smtp_from = smtp_onfig.get('smtp_from')
        smtp_from_name = smtp_onfig.get('smtp_from_name')
        # 格式化邮件内容
        body = Mailer._encode_utf8(body)
        # 邮件类型
        # content_type = 'html' if body.startswith('<html>') else 'plain'
        msg = MIMEMultipart() if files else MIMEText(body, content_type, 'utf-8')
        # 格式化邮件数据
        msg['From'] = formataddr((Mailer._encode_header(smtp_from_name), smtp_from))
        msg['To'] = ', '.join(Mailer._format_list(to_address))
        msg['subject'] = subject

        # 构造附件数据
        if files:
            msg.attach(MIMEText(body, content_type, 'utf-8'))
            cid = 0
            for file_name, payload in files:
                file_name = Mailer._encode_utf8(file_name)
                main_type, sub_type = Mailer._get_file_type(file_name)
                if hasattr(payload, 'read'):
                    payload = payload.read()
                f_name = Mailer._encode_header(file_name)
                mime = MIMEBase(main_type, sub_type, filename=f_name)
                mime.add_header('Content-Disposition', 'attachment', filename=f_name)
                mime.add_header('Content-ID', '<%s>' % cid)
                mime.add_header('X-Attachment-Id', '%s' % cid)
                mime.set_payload(payload)
                encoders.encode_base64(mime)
                msg.attach(mime)
                cid += 1
        if smtp_security == "ssl":
            ssl = True
        else:
            ssl = False
        time_out = 10

        # 没有输入端口则使用默认端口
        if not port or port == 0:
            if ssl:
                port = 465
            else:
                port = 25

        Logger.info('Send mail form %s to %s' % (msg['From'], msg['To']))

        try:
            if ssl:
                # 开启ssl连接模式
                server = smtplib.SMTP_SSL('%s:%s' % (host, port), timeout=time_out)
            else:
                server = smtplib.SMTP('%s:%s' % (host, port), timeout=time_out)
            # 开启调试模式
            if settings.DEBUG:
                server.set_debuglevel(1)

            # 如果存在用户名密码则尝试登录
            if smtp_auth == "Y" and user and passwd:
                server.login(user, passwd)

            # 发送邮件
            server.sendmail(smtp_from, to_address, msg.as_string())

            Logger.info('Mail sent success.')

            # 关闭stmp连接
            server.quit()

        except socket.gaierror as e:
            """ 网络无法连接 """
            raise NetworkError(e)

        except smtplib.SMTPServerDisconnected as e:
            """ 网络连接异常 """
            raise NetworkError(e)

        except smtplib.SMTPException as e:
            """ 邮件发送异常 """
            raise MailerException(e)

    @staticmethod
    def _format_address(s):
        """
        格式化邮件地址
        :param s:string 邮件地址
        :return: string 格式化后的邮件地址
        """
        name, address = parseaddr(s)
        return formataddr((Mailer._encode_header(name), address))

    @staticmethod
    def _encode_header(s):
        """
        格式化符合MIME的头部数据
        :param s: string 待格式化数据
        :return: 格式化后的数据
        """
        return Header(s, 'utf-8').encode()

    @staticmethod
    def _encode_utf8(s):
        """
        格式化成utf-8编码
        :param s: string 待格式化数据
        :return: string 格式化后的数据
        """
        return s.encode('utf-8')

    @staticmethod
    def _get_file_type(file_name):
        """
        获取附件类型
        :param file_name: 附件文件名
        :return: dict 附件MIME
        """
        mimetypes.init()

        r = mimetypes.guess_type(file_name)
        if not r[0]:
            return 'application', 'octet-stream'
        e = r[0].split('/')
        return e[0], e[1]

    @staticmethod
    def _format_list(address):
        """
        将收件人地址格式化成list
        :param address: string/list 收件人邮箱
        :return: list 收件人邮箱list
        """
        l = address
        if isinstance(l, str):
            l = [l]
        return [Mailer._format_address(s) for s in l]


class EMailQueue(object):
    _queque = Queue()

    @staticmethod
    @gen.coroutine
    def start():
        while True:
            msg = yield EMailQueue._queque.get()
            try:
                Mailer.send_mail(msg["to_address"], msg["subject"], msg["body"], msg["content_type"], msg["files"])
                yield gen.sleep(0.01)
            finally:
                EMailQueue._queque.task_done()

    @staticmethod
    @gen.coroutine
    def send(to_address, subject, body, content_type="html", files=None):
        msg = dict()
        msg["to_address"] = to_address
        msg["subject"] = subject
        msg["body"] = body
        msg["content_type"] = content_type
        msg["files"] = files
        yield EMailQueue._queque.put(msg)
