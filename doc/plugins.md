# 自定义插件 #
创建BoPress插件，异常简单，首先在`plugins/`目录下创建一个Python包Demo，然后在Demo包下面创建一个名称为meta的模块，目录结构如下:

    plugins/
		Demo/
	 		__init__.py
			meta.py

在meta.py中我们定义如下要素

    plugin_name = "Demo"
	version = "1.0.0"
	plugin_uri = "domain.com"
	description = "demo plugin"
	author = "you name"
	author_uri = "domain.cn"

> 可在插件包下面任意创建其它模块，注意meta.py作为插件主文件，动作与过滤器尽量在meta.py中添加执行。事实上此包下面的所有模块都会被加载，动作与过滤器可在任何模块中添加执行，但还是建议在meta.py中集中调用，方便维护查找。

> 在plugins目录下已自带一些插件，可提供更详尽的参考。