# -*- coding: utf-8 -*-
from sqlalchemy import Column, String

from bopress.orm import Entity, many_to_many, many_to_one, one_to_one

__author__ = 'yezang'


# 学校
class School(Entity):
    school_id = Column(String(22), nullable=False, primary_key=True)
    name = Column(String(255), nullable=False, default="")


# 老师
@many_to_one(School, True)
class Teacher(Entity):
    teacher_id = Column(String(22), nullable=False, primary_key=True)
    name = Column(String(255), nullable=False, default="")


# 学生
@many_to_many(Teacher)
@many_to_one(School, True)
class Student(Entity):
    student_id = Column(String(22), nullable=False, primary_key=True)
    name = Column(String(255), nullable=False, default="")


# 学生基本信息
@one_to_one(Student)
class StudentBaseInfo(Entity):
    student_base_info_id = Column(String(22), nullable=False, primary_key=True)
    name = Column(String(255), nullable=False, default="")
